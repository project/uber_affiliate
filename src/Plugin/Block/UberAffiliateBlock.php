<?php

namespace Drupal\uber_affiliate\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Displays the top affiliates.
 *
 * @Block(
 *   id = "uber_affiliate_top_users",
 *   admin_label = @Translation("Top Affiliates"),
 * )
 */
class UberAffiliateBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $user = \Drupal::currentUser();
    if ($user->hasPermission('view affiliate overviews') || $user->hasPermission('administer affiliate settings') || $user->hasPermission('view any affiliate info')) {
      return $this->affiliateTopUsersBlockContent($user);
    }
  }

  /**
   * Return block data.
   */
  private function affiliateTopUsersBlockContent($user) {
    $config = \Drupal::config('uber_affiliate.settings');
    $limit = $config->get('affiliate_module_top_users_count_page');
    $time = time();
    $timeInterval = $config->get('affiliate_module_top_users_period_interval');
    $timeLimit = $time - $timeInterval;

    $query = \Drupal::database()->select('affiliate_clicks', 'ac');
    $query->innerjoin('users_field_data', 'u', 'ac.uid = u.uid');
    $query->innerjoin('affiliate', 'aa', 'u.uid = aa.uid');
    $query->condition('aa.active', 1);
    $query->condition('ac.click_time', $timeLimit, '>');
    $query->addExpression('COUNT(DISTINCT ac.clickid)', 'clicks');
    $query->addExpression('MAX(u.uid)', 'uid');
    $query->addExpression('MAX(u.name)', 'name');
    $query->addExpression('MAX(aa.payouts_owed)', 'payouts_owed');
    $query->range(0, $limit);
    $query->groupBy('u.uid');
    $query->orderBy('clicks', 'DESC');
    $results = $query->execute()->fetchAll();

    $results_count = count($results);
    $rows = [];
    $rank = 1;
    if (!empty($results)) {
      foreach ($results as $key => $result) {
        $rowClass = ($key % 2) ? 'affiliate-row-even' : 'affiliate-row-odd';
        $user_data = \Drupal::service('user.data');
        $url = !empty($user_data->get('uber_affiliate', $user->id(), 'affilate_homepage')) ? $user_data->get('uber_affiliate', $user->id(), 'affilate_homepage') : '';

        $name = Link::fromTextAndUrl($result->name, Url::fromUri('internal:/user/' . $result->uid))->toString();
        $rows[] = [
          [
            'class' => [$rowClass, 'affiliate-row-rank'],
            'data' => $rank++,
          ],
          [
            'class' => [$rowClass, 'affiliate-row-name'],
            'data' => $name,
          ],
          [
            'class' => [$rowClass, 'affiliate-row-clicks'],
            'data' => $result->clicks,
          ],
          [
            'class' => [$rowClass, 'affiliate-row-payouts-owed'],
            'data' => $result->payouts_owed,
          ],
        ];
      }
    }
    if (empty($rows)) {
      $rows[] = [
        [
          'data' => $this->t('No affiliate activity found for this time period.'),
          'colspan' => '4',
        ],
      ];
    }

    $header = ['Rank', 'User', 'Clicks', 'Payouts'];
    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['class' => ['top-affiliates-table']],
    ];

    $output = [
      '#markup' => $renderedArray = \Drupal::service('renderer')->render($table),
    ];
    
    $output[] = [
      '#markup' => '<a href="/affiliate/top-users">'. $this->t('More') .'</a>',
    ];

    return $output;
  }

}
