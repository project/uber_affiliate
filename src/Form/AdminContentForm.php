<?php

namespace Drupal\uber_affiliate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Admin Content Form.
 */
class AdminContentForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'uber_affiliate.settings';

  /**
   * Required by FormBase.
   */
  public function getFormId() {
    return 'uber_affiliate_admin_content_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * PayoutForm.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $node_types = node_type_get_names();

    $allow_all_options = [0 => t('No'), 1 => t('Yes')];
    $form['affiliate_module_affiliate_menu_path'] = [
      '#type' => 'textfield',
      '#title' => t('Click-thru menu path'),
      '#default_value' => $config->get('affiliate_module_affiliate_menu_path') ? $config->get('affiliate_module_affiliate_menu_path') : 'affiliate',
      '#description' => t('Many pieces of (good) software exist that will essentially block anything from being displayed inside a browser if the thing being viewed comes from a link that contains words like <em>affiliate</em>, <em>click</em>, <em>partner</em>, <em>ad</em>, etc. This setting allows the menu paths for affiliate click-thru links to be set to something other than the word <em>affiliate</em>.'),
      '#required' => TRUE,
    ];
    $form['affiliate_module_allow_all_paths'] = [
      '#type' => 'radios',
      '#title' => t('Allow all paths'),
      '#default_value' => !is_null($config->get('affiliate_module_allow_all_paths')) ? $config->get('affiliate_module_allow_all_paths') : 0,
      '#options' => $allow_all_options,
      '#description' => t('Select whether affiliates will be credited for referring anyone to any URL from this site (i.e. if an affiliate refers someone to the contact page, they will receive credit for that).'),
    ];
    $form['affiliate_module_allow_all_node_types'] = [
      '#type' => 'radios',
      '#title' => t('Allow all content types'),
      '#default_value' => !is_null($config->get('affiliate_module_allow_all_node_types')) ? $config->get('affiliate_module_allow_all_node_types') : 1,
      '#options' => $allow_all_options,
      '#description' => t('Select whether affiliates will be credited for referring anyone to any piece of content, regardless of its type. Note: a "piece of content", in this case and most others, is a node (i.e. the contact page, any custom views, etc, are not nodes).'),
      '#states' => [
        'invisible' => [
          ':input[name="affiliate_module_allow_all_paths"]' => ['value' => 1],
        ],
      ],
    ];
    $form['affiliate_module_allowed_node_types'] = [
      '#type' => 'checkboxes',
      '#title' => t('Allowed content types'),
      '#default_value' => $config->get('affiliate_module_allowed_node_types') ? $config->get('affiliate_module_allowed_node_types') : [],
      '#options' => $node_types,
      '#description' => t('Select which content types should be automatically available to all affiliates. Each new node will be available when cron is run.'),
      '#states' => [
        'visible' => [
          ':input[name="affiliate_module_allow_all_paths"]' => ['value' => 0],
          ':input[name="affiliate_module_allow_all_node_types"]' => ['value' => 0],
        ],
      ],
    ];
    $form['affiliate_module_affiliate_link_text'] = [
      '#type' => 'textfield',
      '#title' => t('Instructional text'),
      '#default_value' => $config->get('affiliate_module_affiliate_link_text') ? $config->get('affiliate_module_affiliate_link_text') : t('Copy the code below and paste it into your website'),
      '#description' => t('Text instructing users to copy the text in the affiliate link box and paste it into their website in order to earn affiliate credit.'),
    ];
    $form['affiliate_module_flush_all_caches_on_admin_content_form_submit'] = [
      '#type' => 'radios',
      '#title' => t('Flush all caches and rebuild menus after submitting this form'),
      '#default_value' => 0,
      '#options' => [0 => t('No'), 1 => t('Yes')],
      '#description' => t('If you changed the click-thru menu path above, it is highly advised that you flush all the caches and rebuild all the menus after submitting this form. Otherwise there is a very good chance that your affiliate links will break because the system will not know that the click-thru menu path has changed.'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Required by FormBase.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $menu_path_submitted = $form_state->getValue('affiliate_module_affiliate_menu_path');
    $menu_path_current = $config->get('affiliate_module_affiliate_menu_path') ? $config->get('affiliate_module_affiliate_menu_path') : 'affiliate';
    $menu_path_is_dir_or_file = (is_dir(DRUPAL_ROOT . '/' . $menu_path_submitted) || file_exists(DRUPAL_ROOT . '/' . $menu_path_submitted)) ? TRUE : FALSE;
    $pattern = '~^([a-zA-Z0-9_-]+)$~';

    // Remove? Change?
    if (!preg_match($pattern, $menu_path_submitted)) {
      $form_state->setErrorByName('affiliate_module_affiliate_menu_path', t('The affiliate menu path must only contain alphanumeric characters, underscores, and/or dashes.'));
    }
    // Make sure the submitted path does not already exist.
    if (\Drupal::service('path.validator')->isValid($menu_path_submitted) && $menu_path_submitted != $menu_path_current) {
      $form_state->setErrorByName('affiliate_module_affiliate_menu_path', t('This path already exists and cannot be used as the affiliate menu path. Please choose a different path.'));
    }
    if ($menu_path_is_dir_or_file) {
      $form_state->setErrorByName('affiliate_module_affiliate_menu_path', t('This path cannot be used as the affiliate menu path because there is either a directory or a file that exists on your server with the same name. Please choose a different path.'));
    }
  }

  /**
   * Required by FormBase.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $menu_path_submitted = $form_state->getValue('affiliate_module_affiliate_menu_path');
    $menu_path_current = $config->get('affiliate_module_affiliate_menu_path') ? $config->get('affiliate_module_affiliate_menu_path') : 'affiliate';
    $rebuild_caches = (int) $form_state->getValue('affiliate_module_flush_all_caches_on_admin_content_form_submit');

    // Retrieve the submitted form values.
    $values = $form_state->getValues();
    $this->config(static::SETTINGS)
      ->set('affiliate_module_affiliate_menu_path', $values['affiliate_module_affiliate_menu_path'])
      ->set('affiliate_module_allow_all_paths', $values['affiliate_module_allow_all_paths'])
      ->set('affiliate_module_allow_all_node_types', $values['affiliate_module_allow_all_node_types'])
      ->set('affiliate_module_allowed_node_types', $values['affiliate_module_allowed_node_types'])
      ->set('affiliate_module_affiliate_link_text', $values['affiliate_module_affiliate_link_text'])
      ->save();
    if ($rebuild_caches) {
      drupal_flush_all_caches();
      $this->messenger()->addStatus(t('All caches have been cleared and the menu router has been rebuilt to reflect your updated affiliate menu path.'));
    }
    // Remind the admin to rebuild the site's menus
    // if the affiliate menu path has changed.
    elseif ($menu_path_submitted != $menu_path_current) {
      $this->messenger()->addWarning(t('The click-thru menu path appears to have changed, but no caches have been flushed and the menu router has not been updated. If you encounter "Page not found" errors, please consider re-submitting this form but selecting <em>Yes</em> under the <em>Flush all caches</em> option. Doing so will help to ensure that any stale data is removed from the database.'));
    }
    elseif (!\Drupal::service('path.validator')->isValid($menu_path_submitted)) {
      $this->messenger()->addWarning(t('Warning: it appears as though your site requires its caches to be flushed and its menu router to be updated. Until such time, your affiliates will likely encounter "Page not found" errors due to stale information inside the database. To remove the stale information, please consider re-submitting this form but selecting <em>Yes</em> under the <em>Flush all caches</em> option.'));
    }
  }

}
