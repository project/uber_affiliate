<?php

namespace Drupal\uber_affiliate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Payments configuration form.
 */
class PayoutForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'uber_affiliate.settings';

  /**
   * Required by FormBase.
   */
  public function getFormId() {
    return 'uber_affiliate_payout_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * PayoutForm.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['affiliate_module_settings']['payouts'] = [
      '#type' => 'fieldset',
      '#title' => t('Payouts'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['affiliate_module_settings']['payouts']['affiliate_module_payouts_symbol'] = [
      '#type' => 'textfield',
      '#title' => t('Currency symbol'),
      '#default_value' => $config->get('affiliate_module_payouts_symbol') ? $config->get('affiliate_module_payouts_symbol') : '$',
      '#description' => t('Enter the symbol used to denote money in your locale. Example: in the US, France, and the UK, the symbols are typically <b>&dollar;</b>, <b>&euro;</b> and <b>&pound;</b>, respectively.'),
      '#size' => 1,
    ];
    $form['affiliate_module_settings']['payouts']['affiliate_module_payouts_symbol_placement'] = [
      '#type' => 'select',
      '#title' => t('Currency symbol placement'),
      '#default_value' => $config->get('affiliate_module_payouts_symbol_placement') ? $config->get('affiliate_module_payouts_symbol_placement') : 1,
      '#options' => [1 => t('Before'), 2 => t('After')],
      '#description' => t('Select whether the currency symbol should be displayed before or after the amount.'),
    ];
    $form['affiliate_module_settings']['payouts']['affiliate_module_payouts_per_click'] = [
      '#type' => 'textfield',
      '#title' => t('Payouts per click'),
      '#default_value' => !is_null($config->get('affiliate_module_payouts_per_click')) ? $config->get('affiliate_module_payouts_per_click') : 0.00,
      '#description' => t('Enter the amount, using a decimal point as the separator, that each affiliate will receive per click. For example, if the amount is &dollar;0.50 enter <b>0.50</b> in the box. Or, if the amount is 1,07&euro; enter <b>1.07</b> in the box.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Required by FormBase.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Required by FormBase.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get the submitted form values.
    $values = $form_state->getValues();

    // Update the payout settings.
    $config = $this->config(static::SETTINGS)
      ->set('affiliate_module_payouts_symbol', $values['affiliate_module_payouts_symbol'])
      ->set('affiliate_module_payouts_symbol_placement', $values['affiliate_module_payouts_symbol_placement'])
      ->set('affiliate_module_payouts_per_click', $values['affiliate_module_payouts_per_click'])
      ->save();

    // Display a success message.
    \Drupal::messenger()->addStatus($this->t('Settings saved successfully.'));
  }

}
