<?php

namespace Drupal\uber_affiliate\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * PayoutForm.
 */
class PaymentForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  protected static $instanceId;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    if (empty(self::$instanceId)) {
      self::$instanceId = 1;
    }
    else {
      self::$instanceId++;
    }
    return 'uber_affiliate_payment_form' . self::$instanceId;
  }

  /**
   * PayoutForm.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $uid = NULL) {

    $form['affiliate_payout_uid'] = [
      '#type' => 'hidden',
      '#value' => $uid,
    ];
    $form['paid-' . $uid] = [
      '#type' => 'markup',
      '#prefix' => '<div class="affiliate-payout-inlineform-submit" id="paid-' . $uid . '">',
      '#suffix' => '</div>',
    ];
    $form['paid-' . $uid]['affiliate_payout_amount'] = [
      '#prefix' => '<div class="affiliate-payout-inlineform-amount">',
      '#type' => 'number',
      '#step' => '.01',
      '#title' => '',
      '#default_value' => 0.00,
      '#min' => 0,
      '#suffix' => '</div>',
    ];
    $form['affiliate_payout_submit'] = [
      '#type' => 'submit',
      '#value' => t('Apply payment'),
      '#ajax' => [
        'callback' => '::affiliatePayoutInlineFormAjaxSubmit',
        'wrapper' => 'paid-' . $uid,
      ],
    ];
    return $form;
  }

  /**
   * Required by FormBase.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    if (!$user->hasPermission('administer affiliate settings') || !$user->hasPermission('administer affiliate payouts')) {
      return;
    }
    $uid = (int) $form_state->getValue('affiliate_payout_uid');

    $uid_is_affiliate = \Drupal::database()->query("SELECT COUNT(active) FROM {affiliate} WHERE uid = :uid", [":uid" => $uid])->fetchField();
    if (!$uid || !$uid_is_affiliate) {
      $form_state->setErrorByName('affiliate_payout_uid', t('Not a valid affiliate.'));
    }
  }

  /**
   * Required by FormBase.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Required by FormBase.
   */
  public function affiliatePayoutInlineFormAjaxSubmit(array &$form, FormStateInterface $form_state) {
    $uid = (int) $form_state->getValue('affiliate_payout_uid');

    $amount_owed_old = \Drupal::database()->query("SELECT payouts_owed FROM {affiliate} WHERE uid = :uid", [":uid" => $uid])->fetchField();
    $amount_owed_old = number_format((float) $amount_owed_old, 2, '.', '');
    $amount_paid_old = \Drupal::database()->query("SELECT payouts_paid FROM {affiliate} WHERE uid = :uid", [":uid" => $uid])->fetchField();
    $amount_paid_old = number_format((float) $amount_paid_old, 2, '.', '');
    $amount_paid_now = $form_state->getValue('affiliate_payout_amount');
    $amount_paid_now = number_format((float) $amount_paid_now, 2, '.', '');
    // Amounts can be negative... Change?
    $amount_owed_new = $amount_owed_old - $amount_paid_now;
    $amount_owed_new = number_format((float) $amount_owed_new, 2, '.', '');
    $amount_paid_new = $amount_paid_old + $amount_paid_now;
    $amount_paid_new = number_format((float) $amount_paid_new, 2, '.', '');
    \Drupal::database()->update('affiliate')
      ->fields([
        'payouts_owed' => $amount_owed_new,
        'payouts_paid' => $amount_paid_new,
      ])
      ->condition('uid', $uid)
      ->execute();

    $amount = $form_state->getValue('affiliate_payout_amount');
    $element_id = 'paid-' . $uid;
    $element = $form[$element_id];
    $element["#markup"] = t("%amount Applied", ["%amount" => $amount]);
    return $element;
  }

}
