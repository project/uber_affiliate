<?php

namespace Drupal\uber_affiliate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * PayoutForm.
 */
class UserConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'uber_affiliate.settings';

  /**
   * Required by FormBase.
   */
  public function getFormId() {
    return 'uber_affiliate_user_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * PayoutForm.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    // Setting the time intervals for building the options.
    $time_intervals = [
      3600,
      10800,
      21600,
      32400,
      43200,
      86400,
      172800,
      259200,
      604800,
      1209600,
      2419200,
    ];

    $form['affiliate_module_settings']['configuration']['limits'] = [
      '#type' => 'fieldset',
      '#title' => t('Restrictions and limitations'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['affiliate_module_settings']['configuration']['limits']['affiliate_module_allow_all_users'] = [
      '#type' => 'radios',
      '#title' => t('All users are affiliates'),
      '#default_value' => !is_null($config->get('affiliate_module_allow_all_users')) ? $config->get('affiliate_module_allow_all_users') : 0,
      '#options' => [0 => t('No'), 1 => t('Yes')],
      '#description' => t('If selected, anyone with an active account on this site will receive credit for referring an outside user to any path as configured on the <a href="@content_settings_form">content settings form</a>.', ['@content_settings_form' => base_path() . 'admin/config/people/affiliate/content']),
    ];

    $form['affiliate_module_settings']['configuration']['limits']['affiliate_module_check_referrer'] = [
      '#type' => 'radios',
      '#title' => t('Always check for referring URL'),
      '#default_value' => !is_null($config->get('affiliate_module_check_referrer')) ? $config->get('affiliate_module_check_referrer') : 1,
      '#options' => [0 => t('No'), 1 => t('Yes')],
      '#description' => t('If enabled, affiliates will not receive credit unless the user who visits the link first clicks on a link from another website. In other words, if the visitor types the URL directly into their browser instead of clicking on a link somewhere, they will still be taken to the destination page but the affiliate whose link they visited will not receive credit for that visit. Enabled by default.'),
    ];
    $form['affiliate_module_settings']['configuration']['limits']['affiliate_module_click_ignore_enable'] = [
      '#type' => 'radios',
      '#title' => t('Enable IP click ignore'),
      '#default_value' => !is_null($config->get('affiliate_module_click_ignore_enable')) ? $config->get('affiliate_module_click_ignore_enable') : 0,
      '#options' => [0 => t('Disable'), 1 => t('Enable')],
      '#description' => t('If you wish to ignore clicks from the same IP address within a given time period, enable this option.'),
    ];
    $form['affiliate_module_settings']['configuration']['limits']['affiliate_module_click_ignore_interval'] = [
      '#type' => 'select',
      '#title' => t('IP click ignore time limit'),
      '#default_value' => $config->get('affiliate_module_click_ignore_interval') ? $config->get('affiliate_module_click_ignore_interval') : 86400,
      '#options' => $this->buildOptions($time_intervals),
      '#description' => t('Ignore clicks from the same IP address for this time interval.'),
      '#states' => [
        'invisible' => [
          ':input[name="affiliate_module_click_ignore_enable"]' => ['value' => 0],
        ],
      ],
    ];
    $form['affiliate_module_settings']['configuration']['limits']['affiliate_module_ignored_uri_referrers'] = [
      '#type' => 'textarea',
      '#title' => t('Ignore clicks referred by these URLs'),
      '#default_value' => $config->get('affiliate_module_ignored_uri_referrers') ? $config->get('affiliate_module_ignored_uri_referrers') : '',
      '#size' => 70,
      '#rows' => 4,
      '#description' => t('List all referring URL sources, each on a new line, that will be ignored by the affiliate system if a click-thru originates from that source.'),
    ];
    $form['affiliate_module_settings']['configuration']['limits']['affiliate_module_ignored_ips'] = [
      '#type' => 'textarea',
      '#title' => t('Ignore clicks originating from these IP addresses'),
      '#default_value' => $config->get('affiliate_module_ignored_ips') ? $config->get('affiliate_module_ignored_ips') : '',
      '#size' => 70,
      '#rows' => 4,
      '#description' => t('List all IP addresses, each on a new line, that will be ignored by the affiliate system when the IP address of the user who has clicked through matches any of the IP addresses on this list.'),
    ];
    $form['affiliate_module_settings']['configuration']['limits']['affiliate_module_ignored_users'] = [
      '#type' => 'textarea',
      '#title' => t('Denied by User'),
      '#default_value' => $config->get('affiliate_module_ignored_users') ? $config->get('affiliate_module_ignored_users') : '',
      '#size' => 70,
      '#rows' => 4,
      '#description' => t('List all user names, each on a new line, that will be ignored by the affiliate system if a click-thru originates from a user with that same user name. NOTE: currently logged-in users who click on their own links will be ignored automatically, regardless of whether their name appears in this or not.'),
    ];
    $form['affiliate_module_settings']['configuration']['top_users'] = [
      '#type' => 'fieldset',
      '#title' => t('Top Users'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['affiliate_module_settings']['configuration']['top_users']['affiliate_module_top_users_count_page'] = [
      '#type' => 'select',
      '#title' => t('Top Users Count For Page'),
      '#default_value' => $config->get('affiliate_module_top_users_count_page') ? $config->get('affiliate_module_top_users_count_page') : 5,
      '#options' => [5 => 5, 10 => 10, 25 => 25, 50 => 50, 100 => 100],
      '#description' => t('Number of users to show in top users page.'),
    ];
    $form['affiliate_module_settings']['configuration']['top_users']['affiliate_module_top_users_count_block'] = [
      '#type' => 'select',
      '#title' => t('Top Users Count For Block'),
      '#default_value' => $config->get('affiliate_module_top_users_count_block') ? $config->get('affiliate_module_top_users_count_block') : 5,
      '#options' => [5 => 5, 10 => 10, 25 => 25, 50 => 50, 100 => 100],
      '#description' => t('Number of users to show in top users block.'),
    ];

    $time_intervals = [
      86400,
      172800,
      259200,
      259200,
      432000,
      604800,
      1209600,
      2592000,
    ];
    $form['affiliate_module_settings']['configuration']['top_users']['affiliate_module_top_users_period_interval'] = [
      '#type' => 'select',
      '#title' => t('Period for top users block and page'),
      '#default_value' => $config->get('affiliate_module_top_users_period_interval') ? $config->get('affiliate_module_top_users_period_interval') : 259200,
      '#options' => $this->buildOptions($time_intervals),
      '#description' => t('How long of a timespan will be used as the limit for what is shown in the top users page?'),
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * Handler method to return time intervals properly formatted.
   */
  private function buildOptions(array $time_intervals, $granularity = 2, $langcode = NULL) {
    $callback = function ($value) use ($granularity, $langcode) {
      return \Drupal::service('date.formatter')->formatInterval($value, $granularity, $langcode);
    };
    return array_combine($time_intervals, array_map($callback, $time_intervals));
  }

  /**
   * Required by FormBase.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Submit handler for the custom settings form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the submitted form values.
    $values = $form_state->getValues();

    // Save the form values to the state.
    $this->config(static::SETTINGS)
      ->set('affiliate_module_allow_all_users', $values['affiliate_module_allow_all_users'])
      ->set('affiliate_module_check_referrer', $values['affiliate_module_check_referrer'])
      ->set('affiliate_module_click_ignore_enable', $values['affiliate_module_click_ignore_enable'])
      ->set('affiliate_module_click_ignore_interval', $values['affiliate_module_click_ignore_interval'])
      ->set('affiliate_module_ignored_uri_referrers', $values['affiliate_module_ignored_uri_referrers'])
      ->set('affiliate_module_ignored_ips', $values['affiliate_module_ignored_ips'])
      ->set('affiliate_module_ignored_users', $values['affiliate_module_ignored_users'])
      ->set('affiliate_module_top_users_count_page', $values['affiliate_module_top_users_count_page'])
      ->set('affiliate_module_top_users_count_block', $values['affiliate_module_top_users_count_block'])
      ->set('affiliate_module_top_users_period_interval', $values['affiliate_module_top_users_period_interval'])
      ->save();

    // Display a message with the file path on form submission.
    \Drupal::messenger()->addStatus($this->t('Form submitted. Settings saved.'));
  }

}
