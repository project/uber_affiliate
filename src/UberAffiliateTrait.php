<?php

namespace Drupal\uber_affiliate;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Database\Query\TableSortExtender;

/**
 * Uber Affiliate trait.
 */
trait UberAffiliateTrait {
  use DependencySerializationTrait;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Date Formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Sets the Date Formatter service.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The Date Formatter service.
   */
  public function setDateFormatter(DateFormatter $dateFormatter) {
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * Gets the Date Formatter service.
   *
   * @return \Drupal\Core\Datetime\DateFormatter
   *   The Date Formatter service.
   */
  public function getDateFormatter() {
    return $this->dateFormatter;
  }

  /**
   * Gets the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entityTypeManager = $container->get('entity_type.manager');
    $dateFormatter = $container->get('date.formatter');
    $instance = new static();
    $instance->setEntityTypeManager($entityTypeManager);
    $instance->setDateFormatter($dateFormatter);
    return $instance;
  }

  /**
   * Attempt to make sure an incoming click was made validly.
   */
  public function affiliateValidateClick($affid, $dest, $verify_node_type = TRUE) {
    $config = \Drupal::config('uber_affiliate.settings');
    $affid = (int) $affid;
    $dest = UrlHelper::stripDangerousProtocols($dest);

    $node_path_pattern = '<^(node/)([0-9]+)$>';
    $is_node_path = preg_match($node_path_pattern, $dest, $node_path_matches);
    $nid = 0;
    if (!empty($node_path_matches) && isset($node_path_matches[2]) && $node_path_matches[2] > 0) {
      // filter_xss() necessary?
      $nid = (int) Xss::filter($node_path_matches[2]);
    }

    $allow_all_types = $config->get('affiliate_module_allow_all_node_types');
    $allow_all_paths = $config->get('affiliate_module_allow_all_paths');

    if ($allow_all_paths) {
      if (\Drupal::service('path.validator')->isValid($dest) && !UrlHelper::isExternal($dest)) {
        return TRUE;
      }
    }

    if ($allow_all_types || $verify_node_type) {
      if ($is_node_path && $nid) {
        if ($allow_all_types && \Drupal::service('path.validator')->isValid($dest) && !UrlHelper::isExternal($dest)) {
          return TRUE;
        }
        if ($verify_node_type) {
          $node = \Drupal::service('entity_type.manager')->getStorage('node')->load($nid);
          $type = $node->bundle();
          $allowed_node_types = $config->get('affiliate_module_allowed_node_types');
          if (!in_array($type, (array) $allowed_node_types)) {
            return FALSE;
          }
        }
      }
    }
    if (!$this->affiliateValidDestination($dest)) {
      return FALSE;
    }
    if (!$this->affiliateReferrerExists()) {
      return FALSE;
    }
    if (!$this->affiliateValidateUid($affid)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Verify that an affiliate destination path exists, is not external.
   *
   * @param string $dest
   *   The destination, formatted as a relative path
   *   that the user should be redirected to upon validation.
   *
   * @return bool
   *   TRUE if $dest is a valid affiliate path, otherwise FALSE.
   */
  public function affiliateValidDestination($dest) {
    if (!\Drupal::service('path.validator')->isValid($dest)) {
      return FALSE;
    }
    if (UrlHelper::isExternal($dest)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Check to see if the $_SERVER['HTTP_REFERER'] requirements, if any.
   *
   * @return bool
   *   FALSE if referral source is not set, provided the system has not
   *   been told to ignore this setting. Otherwise TRUE.
   */
  public function affiliateReferrerExists() {
    $config = \Drupal::config('uber_affiliate.settings');
    if ($config->get('affiliate_module_check_referrer')) {
      if (!isset($_SERVER['HTTP_REFERER']) || empty($_SERVER['HTTP_REFERER'])) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Generate a url for each affiliate to use based on the current path.
   *
   * @param int $uid
   *   The {users}.uid of the affiliate. If $uid is not provided or
   *   is set to NULL, the value will default to the current user's {users}.uid.
   *
   * @return string
   *   A URL which, when clicked by an outside party, will give the
   *   affiliate credit for a click-thru.
   *
   * @see affiliate_fetch_path()
   * @see affiliateValidateClick()
   * @see affiliate_node_markup()
   */
  public static function affiliateFetchUrl($uid = NULL) {
    $base_url = \Drupal::request()->getSchemeAndHttpHost();
    $user = \Drupal::currentUser();
    $user_id = (int) $user->id();

    if (!empty($uid) && !is_null($uid)) {
      $user_id = (int) $uid;
    }

    $path = self::affiliateFetchPath($user_id);
    $url_raw = $base_url . base_path() . $path;
    $url = UrlHelper::stripDangerousProtocols($url_raw);
    return $url;
  }

  /**
   * Generate a path for each affiliate to use based on the current path.
   *
   * @param int $uid
   *   The {users}.uid of the affiliate. If $uid is not provided or is set to
   *   NULL, the value will default to the current user's {users}.uid.
   *
   * @return string
   *   A path which can be used to generate a clickable URL with which
   *   an affiliate can receive credit.
   *
   * @see affiliateFetchUrl()
   * @see affiliateValidateClick()
   * @see affiliate_node_markup()
   */
  public static function affiliateFetchPath($uid = NULL) {
    $config = \Drupal::config('uber_affiliate.settings');
    $user = \Drupal::currentUser();
    $user_id = (int) $user->id();
    if (!empty($uid) && !is_null($uid)) {
      $user_id = (int) $uid;
    }
    $current_path = \Drupal::service('path.current')->getPath();
    $dest_path = str_replace('/', '|', substr($current_path, 1));
    $menu_prefix = $config->get('affiliate_module_affiliate_menu_path');
    $affiliate_path = $menu_prefix . '/' . $user_id . '/' . $dest_path;
    $out = UrlHelper::stripDangerousProtocols($affiliate_path);
    return $out;
  }

  /**
   * Attempts to fetch a campaign ID based on the visited path.
   */
  public function affiliateGetCampaignId($path) {
    $path = '/' . $path;
    $cid = \Drupal::database()->query("SELECT ac.cid FROM {affiliate_campaigns} ac WHERE ac.redirect_path = :path", [":path" => $path])->fetchField();
    $out = (!empty($cid)) ? (int) $cid : 0;
    return $out;
  }

  /**
   * Record a click-thru, giving credit to the specified affiliate ID.
   *
   * @return void
   *   Credit for the click applied to the affiliate account if validation pass
   *
   * @see affiliate_click_denied()
   */
  public function affiliateAddClick($affid, $campaign_id = 0) {
    $cookie_value = md5(microtime());
    $timeout_flag = TRUE;
    $affid = (int) $affid;
    $campaign_id = (int) $campaign_id;
    $time = time();

    if ($affid) {
      $affiliate_click_denied = $this->affiliateClickDenied($affid);

      if ($_COOKIE && array_key_exists('AFFILIATE', $_COOKIE) && !$_COOKIE['AFFILIATE']) {
        setcookie('AFFILIATE', $cookie_value, \Drupal::time()->getRequestTime() + 86400);
      }
      // Only record the click-thru if it looks like this isn't a cheater and
      // if the user who clicked has the permission to have click counted.
      $user = \Drupal::currentUser();
      if (!$affiliate_click_denied && $user->hasPermission('track affiliate clicks for this role')) {
        \Drupal::database()->insert('affiliate_clicks')
          ->fields([
            'uid' => $affid,
            'cid' => $campaign_id,
            'click_time' => $time,
            'cookie_id' => $cookie_value,
            'ip' => \Drupal::request()->getClientIp(),
            'referrer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
          ])
          ->execute();
      }
    }
  }

  /**
   * Check valid uid.
   */
  public function affiliateValidateUid($affid = 0) {
    $user = \Drupal::currentUser();

    $affid = (int) $affid;
    $uid = (int) $user->id();
    $valid = TRUE;

    // Fail if affiliate ID from the URL is the same as the currently
    // logged-in user ID.
    if ($affid == $uid) {
      $valid = FALSE;
    }
    // Fail if affiliate ID is not set or is set to 0
    // (which, essentially, means that it's not set).
    if (empty($affid) || $affid < 1) {
      $valid = FALSE;
    }
    return $valid;
  }

  /**
   * Check if clicks.
   */
  public function affiliateClickDenied($uid) {
    $account = \Drupal::service('entity_type.manager')->getStorage('user')->load($uid);
    $config = \Drupal::config('uber_affiliate.settings');
    $denied_users = explode("\n", $config->get('affiliate_module_ignored_users'));

    if (in_array($account->name, $denied_users)) {
      return TRUE;
    }
    $ip = \Drupal::request()->getClientIp();
    $denied_ips = explode("\n", $config->get('affiliate_module_ignored_ips'));
    if (in_array($ip, $denied_ips)) {
      return TRUE;
    }

    if (!$this->affiliateReferrerExists()) {
      return TRUE;
    }
    $too_soon = $this->affiliateCheckTimer();
    if ($too_soon) {
      return TRUE;
    }
    $denied_uri_referrers = explode("\n", $config->get('affiliate_module_ignored_uri_referrers'));
    if (count($denied_uri_referrers)) {
      foreach ($denied_uri_referrers as $referrer) {
        if (!empty($referrer) && strpos($_SERVER['HTTP_REFERER'], $referrer)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Check timer.
   */
  public function affiliateCheckTimer() {
    $config = \Drupal::config('uber_affiliate.settings');
    $enabled = $config->get('affiliate_module_click_ignore_enable');
    if (!$enabled) {
      return FALSE;
    }
    $ip = \Drupal::request()->getClientIp();
    // Default time limit allows for one click per IP per day.
    $time = time();
    $interval = $config->get('affiliate_module_click_ignore_interval');
    $limit = $time - $interval;
    return (bool) \Drupal::database()->query("SELECT COUNT(*) FROM {affiliate_clicks} 
      WHERE ip = :ip AND click_time > :limit",
      [":ip" => $ip, ":limit" => $limit])->fetchField();
  }

  /**
   * Get content of affiliate user history.
   */
  public function affiliateUserPageOutput($acctid) {
    $out = $this->affiliateUserPageOutputTop($acctid);
    $out += $this->affiliateUserPageOutputDetails($acctid);
    return $out;
  }

  /**
   * Get data.
   */
  public function affiliateUserPageOutputDetails($acctid) {
    $header = [
      ['data' => t('Date'), 'field' => 'click_time', 'sort' => 'desc'],
      ['data' => t('Referrer'), 'field' => 'referrer'],
    ];

    $count_query = \Drupal::database()->select('affiliate_clicks')
      ->condition('uid', $acctid);
    $count_query->addExpression('COUNT(DISTINCT(clickid))');

    $query = \Drupal::database()->select('affiliate_clicks', 'a')
      ->fields('a', ['click_time', 'referrer'])
      ->condition('uid', $acctid)
      ->extend(PagerSelectExtender::class)
      ->limit(10)
      ->extend(TableSortExtender::class)
      ->orderByHeader($header);
    $query->setCountQuery($count_query);
    $result = $query->execute();

    $rows = [];
    foreach ($result as $res) {
      $click_time = \Drupal::service('date.formatter')->format($res->click_time);
      $referrer = !empty($res->referrer) ? $res->referrer : '';
      $rows[] = [$click_time, $referrer];
    }

    $build['affiliate_bottom_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No click information available.'),
    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];
    return $build;
  }

  /**
   * Get affiliate header content.
   */
  public function affiliateUserPageOutputTop($acctid) {
    $header = [
      ['data' => t('Active')],
      ['data' => t('Affiliate since')],
      ['data' => t('Clicks')],
      ['data' => t('Referrals')],
      ['data' => t('Payouts owed')],
      ['data' => t('Payouts paid')],
    ];
    $config = \Drupal::config('uber_affiliate.settings');

    $payouts_symbol = $config->get('affiliate_module_payouts_symbol');
    $payouts_symbol_pos = $config->get('affiliate_module_payouts_symbol_placement');
    $payouts_amount = $config->get('affiliate_module_payouts_per_click');

    $query = \Drupal::database()->select('affiliate', 'a')
      ->condition('uid', $acctid)
      ->fields('a', [
        'uid',
        'active',
        'created',
        'clicks',
        'referrals',
        'payouts_owed',
        'payouts_paid',
      ]);
    $result = $query->execute();

    $rows = [];
    foreach ($result as $affiliate) {
      $affiliate_uid = $affiliate->uid;
      $affiliate_name = $this->entityTypeManager->getStorage('user')->load($affiliate_uid)->getDisplayName();
      $affiliate_uid_display = Link::fromTextAndUrl(
        Html::escape($affiliate_uid . ' - ' . $affiliate_name),
        Url::fromRoute('entity.user.canonical', ['user' => $affiliate_uid])
      )->toString();
      $affiliate_active = $affiliate->active ? t('Yes') : t('No');
      $affiliate_created = $this->dateFormatter->format($affiliate->created);
      $payouts_owed_display = $payouts_symbol . $affiliate->payouts_owed;
      $payouts_paid_display = $payouts_symbol . $affiliate->payouts_paid;
      if ($payouts_symbol_pos == 2) {
        $payouts_owed_display = $affiliate->payouts_owed . $payouts_symbol;
        $payouts_paid_display = $affiliate->payouts_paid . $payouts_symbol;
      }
      $rows[] = [
        $affiliate_active,
        $affiliate_created,
        $affiliate->clicks,
        $affiliate->referrals,
        $payouts_owed_display,
        $payouts_paid_display,
      ];
    }

    $build['affiliate_top_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No affiliate information available.'),
    ];

    return $build;
  }

}
