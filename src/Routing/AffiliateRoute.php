<?php

namespace Drupal\uber_affiliate\Routing;

use Symfony\Component\Routing\Route;

/**
 * Class EntityOverview.
 *
 * @package Drupal\uber_affiliate\Routing
 */
class AffiliateRoute {

  /**
   * Dynamically generate the routes for the entity details.
   *
   * @return array
   *   Return the actual route
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function routes() {
    $config = \Drupal::config('uber_affiliate.settings');

    $affiliate_menu_path = $config->get('affiliate_module_affiliate_menu_path');
    $routes = [];
    $routes['affiliate'] = new Route(
      $affiliate_menu_path . '/' . '{aff_id}' . '/' . '{dest_path}' . '/' . '{tracker_id}',
      [
        '_controller' => 'Drupal\uber_affiliate\Controller\UberAffiliate::affiliatePage',
        'dest_path' => 'fallback',
        'tracker_id' => 'fallback',
      ],
      [
        '_permission'  => 'track affiliate clicks for this role',
      ],
      );
    return $routes;
  }

}
