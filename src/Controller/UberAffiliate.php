<?php

namespace Drupal\uber_affiliate\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\Query\TableSortExtender;
use Drupal\Core\Render\RendererInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\uber_affiliate\UberAffiliateTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Database\Query\PagerSelectExtender;

/**
 * Affiliate Overview page class.
 */
class UberAffiliate extends ControllerBase {
  use UberAffiliateTrait;

  /**
   * Admin overview page method.
   */
  public function affiliateAdminOverviewpage() {
    $account = \Drupal::currentUser();
    $renderer = \Drupal::service('renderer');
    $payouts_symbol = '';
    $payouts_symbol_pos = '';

    $header = [
      ['data' => t('ID'), 'field' => 'uid', 'sort' => 'desc'],
      ['data' => t('Name'), 'field' => 'name'],
      ['data' => t('Active'), 'field' => 'active'],
      ['data' => t('Created'), 'field' => 'created'],
      ['data' => t('Clicks'), 'field' => 'clicks'],
      ['data' => t('Referrals'), 'field' => 'referrals'],
      ['data' => t('Payouts owed'), 'field' => 'payouts_owed'],
      ['data' => t('Payouts paid'), 'field' => 'payouts_paid'],
      ['data' => t('operation')],
    ];

    $database = \Drupal::database();
    $query = $database->select('affiliate', 'a');
    $query->innerjoin('users_field_data', 'u', 'u.uid=a.uid');
    $query->condition('a.active', 1);
    $query->fields('u', ['uid', 'name', 'status', 'created', 'access']);
    $query->fields('a', ['active', 'clicks', 'referrals', 'payouts_owed', 'payouts_paid']);
    $query->extend(TableSortExtender::class)->orderByHeader($header);
    $pager = $query->extend(PagerSelectExtender::class)->limit(30);
    $result = $pager->execute();

    $rows = [];
    foreach ($result as $affiliate) {
      $affiliate_uid = $affiliate->uid;

      $affiliate_name = Link::fromTextAndUrl(Html::escape($affiliate->name), Url::fromUri('internal:/user/' . $affiliate->uid))->toString();    
      $affiliate_active = $affiliate->active ? t('Yes') : t('No');
      $affiliate_created = \Drupal::service('date.formatter')->format($affiliate->created);

      $payouts_owed_display = $payouts_symbol . $affiliate->payouts_owed;
      $payouts_paid_display = $payouts_symbol . $affiliate->payouts_paid;
      $payouts_form = '';
      if ($payouts_symbol_pos == 2) {
        $payouts_owed_display = $affiliate->payouts_owed . $payouts_symbol;
        $payouts_paid_display = $affiliate->payouts_paid . $payouts_symbol;
      }

      if (AccessResult::allowedIf($account->hasPermission('administer affiliate settings')) || AccessResult::allowedIf($account->hasPermission('administer affiliate payouts'))) {
        $form = \Drupal::formBuilder()->getForm('Drupal\uber_affiliate\Form\PaymentForm', $affiliate_uid);
        $payouts_form = \Drupal::service('renderer')->render($form);
      }
      $rows[] = [
        $affiliate->uid,
        $affiliate_name,
        $affiliate_active,
        $affiliate_created,
        $affiliate->clicks,
        $affiliate->referrals,
        $payouts_owed_display,
        $payouts_paid_display,
        $payouts_form,
      ];
    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No content has been found.'),
      '#attached' => [
        'library' => ['uber_affiliate/affiliate_css'],
      ],
    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];

    return [
      '#type' => '#markup',
      '#markup' => \Drupal::service('renderer')->render($build),
    ];
  }

  /**
   * Displays the referral information of the said user.
   *
   * @param int $uid
   *   User ID.
   *
   * @return mixed
   *   mixed values
   *   Rendered array of the users information.
   */
  public function uberAffiliateUserPage($uid) {
    $currentUser = \Drupal::currentUser();
    $currentUserid = $currentUser->id();
    $acctid = $uid;
    if (($currentUserid == $acctid && $currentUser->hasPermission('view own affiliate info')) || $currentUser->hasPermission('administer affiliate settings')) {
      $name = $currentUser->getAccountName();
      $output = $this->affiliateUserPageOutput($acctid);
      $title = $this->t('Affiliate information for @name', ['@name' => $name]);
      $renderedArray = \Drupal::service('renderer')->render($output);

      return [
        '#markup' => $renderedArray,
        '#title' => $title,
      ];
    }
    else {
      return new Response(t('Access denied'), 403);
    }
  }

  /**
   * Displays the top users.
   *
   * @return mixed
   *   Return mixed values.
   */
  public function uberAffiliateTopUsersPage() {
    $user = \Drupal::currentUser();
    $config = \Drupal::config('uber_affiliate.settings');
    $limit = $config->get('affiliate_module_top_users_count_page');
    $time = time();
    $timeInterval = $config->get('affiliate_module_top_users_period_interval');
    $timeLimit = $time - $timeInterval;

    $query = Database::getConnection()->select('affiliate_clicks', 'ac');
    $query->innerjoin('users_field_data', 'u', 'ac.uid = u.uid');
    $query->innerjoin('affiliate', 'aa', 'u.uid = aa.uid');
    $query->condition('aa.active', 1);
    $query->condition('ac.click_time', $timeLimit, '>');
    $query->addExpression('COUNT(DISTINCT ac.clickid)', 'clicks');
    $query->addExpression('MAX(u.uid)', 'uid');
    $query->addExpression('MAX(u.name)', 'name');
    $query->addExpression('MAX(aa.payouts_owed)', 'payouts_owed');
    $query->range(0, $limit);
    $query->groupBy('u.uid');
    $query->orderBy('clicks', 'DESC');
    $results = $query->execute()->fetchAll();

    $rows = [];
    $rank = 1;
    if (!empty($results)) {
      foreach ($results as $key => $result) {
        $rowClass = ($key % 2) ? 'affiliate-row-even' : 'affiliate-row-odd';
        $user_data = \Drupal::service('user.data');
        $url = !empty($user_data->get('uber_affiliate', $user->id(), 'affilate_homepage')) ? $user_data->get('uber_affiliate', $user->id(), 'affilate_homepage') : '';

        $name = Link::fromTextAndUrl($result->name, Url::fromUri('internal:/user/' . $result->uid))->toString();
        $rows[] = [
          [
            'class' => [$rowClass, 'affiliate-row-rank'],
            'data' => $rank++,
          ],
          [
            'class' => [$rowClass, 'affiliate-row-name'],
            'data' => $name,
          ],
          [
            'class' => [$rowClass, 'affiliate-row-clicks'],
            'data' => $result->clicks,
          ],
          [
            'class' => [$rowClass, 'affiliate-row-payouts-owed'],
            'data' => $result->payouts_owed,
          ],
        ];
      }
    }
    if (empty($rows)) {
      $rows[] = [
        [
          'data' => $this->t('No affiliate activity found for this time period.'),
          'colspan' => '4',
        ],
      ];
    }

    $header = ['Rank', 'User', 'Clicks', 'Payouts'];
    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['class' => ['top-affiliates-table']],
    ];

    $build = [
      '#markup' => $renderedArray = \Drupal::service('renderer')->render($table),
    ];

    $timeLimitFormatted = date('Y-m-d', $timeLimit);
    $timeFormatted = date('Y-m-d', $time);
    $title = $this->t('Top affiliates from @from through @to', [
      '@from' => $timeLimitFormatted,
      '@to' => $timeFormatted,
    ]);

    $build['#title'] = $title;
    return $build;
  }

  /**
   * The primary tracking of the affiliate and the main affiliate link.
   *
   * @todo Fix problem
   *   Have this function checked by security team if possible, or if not,
   *   get a second opinion just to be sure.
   */
  public function affiliatePage($aff_id, $dest_path, $tracker_id) {
    $config = \Drupal::config('uber_affiliate.settings');
    $allow_all_paths = $config->get('affiliate_module_allow_all_paths');
    $allow_all_node_types = $config->get('affiliate_module_allow_all_node_types');

    $affid = (int) Xss::filter($aff_id);
    $path_given = $dest_path;
    $tracker = (int) Xss::filter($tracker_id);

    // Note to self: ask a security guru about this.
    $path_arg = UrlHelper::stripDangerousProtocols(implode('/', explode('|', rawurldecode($path_given))));

    $verify_node_type = FALSE;
    if (!$allow_all_node_types) {
      $verify_node_type = TRUE;
    }
    if ($allow_all_paths) {
      $verify_node_type = FALSE;
    }

    if (empty($tracker)) {
      $tracker = 0;
    }
    if (empty($affid)) {
      $affid = 1;
    }

    // Send invalid clicks to the front page if no path argument is supplied.
    if (empty($path_arg)) {
      $path = base_path();
    }
    else {
      $path = $path_arg;
    }

    // Path aliases will fail validation... Convert to normal path.
    $path_normal = \Drupal::service('path_alias.manager')->getPathByAlias($path);

    // This seems like it could be written better...?
    if ($this->affiliateValidateClick($affid, $path_normal, $verify_node_type)) {
      $campaign_id = $this->affiliateGetCampaignId($path_normal);
      $this->affiliateAddClick($affid, $campaign_id);
    }
    $destination_is_valid = $this->affiliateValidDestination($path_normal);

    // Empty will simply take us to the front page.
    $destination_actual = '';
    if ($destination_is_valid) {
      $destination_actual = \Drupal::service('path_alias.manager')->getPathByAlias($path_normal);
    }

    // Unnecessary check_url()? Better safe than sorry...
    $destination_actual = UrlHelper::stripDangerousProtocols($destination_actual);

    // At minimum, if all validations fail, we'll be taken to the front page.
    // If the path given is valid though, we'll be taken to that path either
    // way, regardless of whether the affiliate was credited with
    // a valid click-thru.
    if (empty($destination_actual)) {
      $language = \Drupal::languageManager()->getLanguage('vi');
      $url = Url::fromRoute('<front>', [], ['language' => $language]);
      return new RedirectResponse($url->toString());

    }
    return new RedirectResponse('/' . $destination_actual);
  }

  /**
   * Renders a form using the renderer service.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   *
   * @return string
   *   The rendered form HTML.
   */
  public function renderPlain($form, RendererInterface $renderer) {
    $output = $renderer->renderRoot($form);
    $renderer->renderPlain($form);
    return $output;
  }

}
